from setuptools import setup, find_packages

setup(
    name='appyx',
    version='0.0.15',
    description='Framework for building commercial applications',
    url='https://gitlab.com/eryx/appyx',
    author='Eryx Team',
    author_email='info@eryx.co',
    license='MIT',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.7',
        'Topic :: Software Development',
    ],
    packages=find_packages(),
    install_requires=[],
)
