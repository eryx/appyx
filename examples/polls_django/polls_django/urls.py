"""polls_django URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from examples.polls_django.polls.polls_without_django.polls_environment import PollsEnvironment


class DjangoEndpointAdapter:
    def __init__(self, endpoint):
        self._endpoint = endpoint

    def url_pattern(self):
        name = self._endpoint.name()

        route_pattern = self._endpoint.interface().route_pattern_for_endpoint_named(name)

        django_view = self._new_django_view()
        return path(route_pattern, django_view, name=name)

    def _new_django_view(self):
        handler = self._endpoint.handler()

        authenticators = [
            authentication_method.authenticator()
            # Nota: esta parte es dudosa, porque los authentication methods SON de Django.
            #  Lo cual hace que el core esté acoplado a Django implícitamente.
            #  Tal vez cuando tengamos otro mecanismo de autenticación que no sea el de Django
            #  podamos generalizar o extender este modelo y ahi si hacer algo desacoplado.
            for authentication_method in self._endpoint.get_authentication_methods()
        ]

        django_view = handler.as_view(authenticators=authenticators)
        return django_view


class DjangoPaths:
    """
    Models a factory for Django paths.
    It is responsible for obtaining the paths Django uses in urls.py from a given interface.
    """
    def __init__(self, application):
        self._application = application

    def value(self):
        interfaces = self._application.interfaces().exposed_http_interfaces()

        all_url_patterns = []
        for interface in interfaces:
            all_url_patterns += self._url_patterns_for_interface(interface)
        return all_url_patterns

    def _url_patterns_for_interface(self, interface):
        return [DjangoEndpointAdapter(endpoint).url_pattern() for endpoint in interface.endpoints()]


urlpatterns = [
    path('admin/', admin.site.urls),
]

urlpatterns += DjangoPaths(PollsEnvironment().current_app()).value()
