from rest_framework.views import APIView


class DjangoRestFrameworkHandlerAdapter(APIView):
    authenticators = []

    def set_handler(self, appyx_handler):
        self._handler = appyx_handler

    def get(self, request):
        django_response = self._handler.get(request)
        return django_response
