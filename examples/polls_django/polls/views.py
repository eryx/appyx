from rest_framework.response import Response

from appyx.layers.interface.handlers.appyx_http_handler import AppyxHttpHandler
from examples.polls_django.polls.polls_without_django.polls_environment import PollsEnvironment


# Create your views here.
class ListPollsHandler(AppyxHttpHandler):
    authenticators = None

    def get(self, request):
        environment = PollsEnvironment()
        app = environment.current_app()
        business = app.current_business()
        if len(business.latest_questions()) == 0:
            poll_index_message = "Hi! <div>There are no questions at this time.</div>"
        else:
            poll_index_message = "Hi! <div>You're at the polls index. Questions: </div>"
            for question in business.latest_questions():
                poll_index_message += f"<div>{question.text()}</div>"
        return Response(poll_index_message)


class CreateQuestionHandler(AppyxHttpHandler):
    def post(self, request):
        environment = PollsEnvironment()
        app = environment.current_app()
        business = app.current_business()

        business.create_question_with_choices(request.POST['question'], ["Not much", "The sky", "Just hacking again"])

        return Response('')
