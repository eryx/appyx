from django.test import TestCase
from django.urls import reverse

from examples.polls_django.polls.polls_without_django.interface.list_polls_handler import HttpResponse
from examples.polls_django.polls.polls_without_django.polls_environment import PollsEnvironment


class TestXXX(TestCase):
    def setUp(self) -> None:
        super(TestXXX, self).setUp()
        PollsEnvironment().reset_current_app()

    def test_01_with_django_list_empty_questions(self):
        self._use_scenario_empty_polls()

        endpoint = self._polls_interface.endpoint_named('index_with_django')
        handler = endpoint.handler_for('GET')
        django_request = None
        django_response = handler.get(django_request)
        response_body = django_response.data
        self.assertEqual("Hi! <div>There are no questions at this time.</div>", response_body)
        self.assertEqual(200, django_response.status_code)

    def test_01_without_django_list_empty_questions(self):
        self._use_scenario_empty_polls()

        endpoint = self._polls_interface.endpoint_named('index_without_django')
        handler = endpoint.handler_for('GET')
        django_request = None
        django_response = handler.get(django_request)
        response_body = django_response.data
        self.assertEqual("Hi! <div>There are no questions at this time.</div>", response_body)
        self.assertEqual(200, django_response.status_code)

    def test_02_with_django_list_one_question(self):
        self._use_scenario_with_one_question()

        endpoint = self._polls_interface.endpoint_named('index_with_django')
        handler = endpoint.handler_for('GET')
        django_request = None
        django_response = handler.get(django_request)
        response_body = django_response.data
        self.assertEqual("Hi! <div>You're at the polls index. Questions: </div><div>What's up</div>", response_body)
        self.assertEqual(200, django_response.status_code)

    def test_02_without_django_list_one_question(self):
        self._use_scenario_with_one_question()

        endpoint = self._polls_interface.endpoint_named('index_without_django')
        handler = endpoint.handler_for('GET')
        django_request = None
        django_response = handler.get(django_request)
        response_body = django_response.data
        self.assertEqual("Hi! <div>You're at the polls index. Questions: </div><div>What's up</div>", response_body)
        self.assertEqual(200, django_response.status_code)

    def test_03_with_django_create_question(self):
        self._use_scenario_empty_polls()
        endpoint = self._polls_interface.endpoint_named('create_question_with_django')
        handler = endpoint.handler_for('POST')
        question_text = "What's up"

        django_response = self.client.post(reverse('create_question_with_django'), data={'question': question_text})
        appyx_response = HttpResponse(django_response.data)

        self.assertEqual(200, appyx_response.status_code)
        self.assertEqual("", appyx_response.data)


        latest_question = self._business.latest_questions()[0]
        self.assertEqual(question_text, latest_question.text())

    # Scenarios
    def _use_scenario_with_one_question(self):
        self._setup_base_scenario()
        self._business.create_question_with_choices("What's up", ["Not much", "The sky", "Just hacking again"])

    def _use_scenario_empty_polls(self):
        self._setup_base_scenario()

    def _setup_base_scenario(self):
        self._app = PollsEnvironment().current_app()
        self._business = self._app.current_business()
        self._polls_interface = self._app.interfaces().web_interface()
