from examples.polls_django.polls.polls_without_django.polls_environment import PollsEnvironment


class HttpResponse:
    def __init__(self, content):
        self._content = content

    @property
    def data(self):
        return self._content

    @property
    def status_code(self):
        return 200


class ListPollsHandlerWithoutDjango:

    def get(self, request):
        environment = PollsEnvironment()
        app = environment.current_app()
        business = app.current_business()
        if len(business.latest_questions()) == 0:
            poll_index_message = "Hi! <div>There are no questions at this time.</div>"
        else:
            poll_index_message = "Hi! <div>You're at the polls index. Questions: </div>"
            for question in business.latest_questions():
                poll_index_message += f"<div>{question.text()}</div>"
        return HttpResponse(poll_index_message)

    class CreateQuestionHandler:

        def post(self, request):
            environment = PollsEnvironment()
            app = environment.current_app()
            business = app.current_business()
            if len(business.latest_questions()) == 0:
                poll_index_message = "Hi! <div>There are no questions at this time.</div>"
            else:
                poll_index_message = "Hi! <div>You're at the polls index. Questions: </div>"
                for question in business.latest_questions():
                    poll_index_message += f"<div>{question.text()}</div>"
            return HttpResponse(poll_index_message)
