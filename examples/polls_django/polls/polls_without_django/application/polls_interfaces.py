from appyx.layers.interface.interfaces.appyx_endpoint import AppyxEndpoint
from appyx.layers.interface.interfaces.interfaces import HTTPInterface
from examples.polls_django.polls.django_rest_framework_handler_adapter import DjangoRestFrameworkHandlerAdapter


class PollsRespondentWebInterface(HTTPInterface):
    def description(self):
        return 'Interfaz web para cuestionarios'

    def version(self):
        pass

    def _initialize_sections(self):
        pass

    def _route_prefix(self):
        return 'polls'

    def endpoints(self):
        return [self.index_with_django(), self.index_without_django(), self.create_question_with_django()]

    def index_with_django(self):
        from examples.polls_django.polls.views import ListPollsHandler
        route_pattern = 'index_with_django'
        name = 'index_with_django'
        endpoint = AppyxEndpoint(ListPollsHandler(), route_pattern, name, self)
        return endpoint

    def index_without_django(self):
        from examples.polls_django.polls.polls_without_django.interface.list_polls_handler import \
            ListPollsHandlerWithoutDjango
        handler = ListPollsHandlerWithoutDjango()
        adapter = DjangoRestFrameworkHandlerAdapter()
        adapter.set_handler(handler)
        route_pattern = 'index_without_django'
        name = 'index_without_django'
        endpoint = AppyxEndpoint(adapter, route_pattern, name, self)
        return endpoint

    def create_question_with_django(self):
        from examples.polls_django.polls.views import CreateQuestionHandler
        route_pattern = 'create_question_with_django'
        name = 'create_question_with_django'
        endpoint = AppyxEndpoint(CreateQuestionHandler(), route_pattern, name, self)
        return endpoint


class PollsInterfaces:
    def exposed_http_interfaces(self):
        return [self.web_interface()]

    def web_interface(self):
        return PollsRespondentWebInterface(
            name='Polls respondent web',
            base_url='',
            default_authentication_methods=[])
